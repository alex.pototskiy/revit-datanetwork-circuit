﻿#pragma checksum "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "CB4AB4427E9F8C9E2FCD45AE8CA29E337BD252822728C4F464A7595AD95A170F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using REX.Common;
using REX.Controls.WPF;
using REX.FillDataCircuitInfo.Main.MyData;
using REX.FillDataCircuitInfo.Util;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace REX.FillDataCircuitInfo.Resources.Dialogs {
    
    
    /// <summary>
    /// PortConfiguration
    /// </summary>
    public partial class PortConfiguration : REX.Common.REXExtensionControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 28 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal REX.Controls.WPF.REXEditBox NumberOfAggregatePorts;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox SrcAndDestSameNumbering;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal REX.Controls.WPF.REXComboBox SrcPortNumberType;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal REX.Controls.WPF.REXComboBox DstPortNumberType;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FillDataCircuitInfo;component/resources/dialogs/portconfiguration.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.NumberOfAggregatePorts = ((REX.Controls.WPF.REXEditBox)(target));
            return;
            case 2:
            this.SrcAndDestSameNumbering = ((System.Windows.Controls.CheckBox)(target));
            
            #line 41 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
            this.SrcAndDestSameNumbering.Checked += new System.Windows.RoutedEventHandler(this.SrcAndDestSameNumbering_OnChecked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.SrcPortNumberType = ((REX.Controls.WPF.REXComboBox)(target));
            
            #line 47 "..\..\..\..\..\Resources\Dialogs\PortConfiguration.xaml"
            this.SrcPortNumberType.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SrcPortNumberType_OnSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DstPortNumberType = ((REX.Controls.WPF.REXComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

