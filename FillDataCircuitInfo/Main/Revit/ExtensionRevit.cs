//
// (C) Copyright 2007-2019 by Autodesk, Inc. All rights reserved.
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM 'AS IS' AND WITH ALL ITS FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE. AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;
using Autodesk.REX.Framework;
using REX.Common;
using REX.Controls.WPF;
using REX.FillDataCircuitInfo.Kotlin;
using REX.FillDataCircuitInfo.Main.MyData;
using REX.FillDataCircuitInfo.Resources.Dialogs;
using WPFGrid = System.Windows.Controls.Grid;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    internal class ExtensionRevit : REXExtensionProduct
    {
        public CircuitsSet CircuitsSet;
        public CircuitParameters CircuitParameters;
        public CircuitFamilies CircuitFamilies;

        public ExtensionRevit(REX.Common.REXExtension Ext)
            : base(Ext)
        {
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData => ThisMainExtension.Data;

        #region REXExtensionProduct Members

        public override bool OnInitialize()
        {
            CircuitsSet = new CircuitsSet(ThisMainExtension.Revit.ActiveUIDocument.Document);
            CircuitParameters = new CircuitParameters(ThisMainExtension.Revit.ActiveUIDocument.Document, CircuitsSet);
            CircuitFamilies = new CircuitFamilies(CircuitsSet);

            return true;
        }

        public override void OnCreate()
        {
            // insert code here.
        }

        public override void OnCreateDialogs()
        {
            SetupInputParametersGrid();
            // Insert code here
        }

        public override void OnCreateLayout()
        {
            // insert code here.
        }

        public override void OnSetDialogs()
        {
            // insert code here.
        }

        private void SetupInputParametersGrid()
        {
            var grid = ThisMainExtension.InputParametersControl.InputParameters;
            var row = 0;
            foreach (var circuitFamily in CircuitFamilies.Families)
            {
                grid.RowDefinitions.Add(new RowDefinition() {Height = GridLength.Auto});
                var label = new TextBlock()
                {
                    Text = circuitFamily.Key,
                    VerticalAlignment = VerticalAlignment.Center,
                    TextWrapping = TextWrapping.Wrap
                };
                WPFGrid.SetRow(label, row);
                WPFGrid.SetColumn(label, 0);
                grid.Children.Add(label);
                var source = circuitFamily.Value.Parameters.Keys.OrderBy(it => it).ToList();
                var deviceName = new REXComboBox()
                {
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    ItemsSource = source,
                    Margin = new Thickness
                    {
                        Right = 4
                    }
                };
                WPFGrid.SetRow(deviceName, row);
                WPFGrid.SetColumn(deviceName, 1);
                grid.Children.Add(deviceName);
                var deviceType = new REXComboBox()
                {
                    ItemsSource = source,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    Margin = new Thickness
                    {
                        Right = 4
                    }
                };
                WPFGrid.SetRow(deviceType, row);
                WPFGrid.SetColumn(deviceType, 2);
                grid.Children.Add(deviceType);
                var firstPortNumber = new REXComboBox()
                {
                    ItemsSource = circuitFamily.Value.IntParameterNames,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                };
                WPFGrid.SetRow(firstPortNumber, row);
                WPFGrid.SetColumn(firstPortNumber, 3);
                grid.Children.Add(firstPortNumber);
                ThisMainExtension.InputParametersControl.ParameterControls[circuitFamily.Key] =
                    new InputParameterControls
                    {
                        DeviceName = deviceName,
                        DeviceType = deviceType,
                        FirstPortNumber = firstPortNumber
                    };
                row++;
            }
        }

        public override void OnSetData()
        {
            // insert code here.
        }

        public override void OnActivateLayout(REX.Common.REXLayoutItem LItem, bool Activate)
        {
            // insert code here.
        }

        public override bool OnCanRun()
        {
            // insert code here.

            return true;
        }

        public override void OnRun()
        {
            var panelRegEx = new Regex(ThisMainData.SelectionData.PanelNamePattern);
            foreach (var electricalSystem in CircuitsSet.Circuits.Where(it => panelRegEx.IsMatch(it.PanelName)))
            {
                var distribution = ExtractDeviceInfo(electricalSystem, electricalSystem.BaseEquipment.Id);
                var targetId = electricalSystem.ConnectorManager.Connectors.Cast<Connector>()
                    .SelectMany(it => it.AllRefs.Cast<Connector>())
                    .Single(it => it.Owner.Id != electricalSystem.BaseEquipment.Id).Owner.Id;
                var target = ExtractDeviceInfo(electricalSystem, targetId);
                var dataCircuitInfo = CreateDataCircuitInfo(distribution, target);
                WriteCircuitInfo(electricalSystem, dataCircuitInfo);
            }
        }

        private void WriteCircuitInfo(ElectricalSystem ElectricalSystem, DataCircuitInfo DataCircuitInfo)
        {
            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.SrcDeviceId, out var param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.DistributionalDevice.Name);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.SrcDeviceType, out param))
            {
                ElectricalSystem.get_Parameter((param.Definition)).Set(DataCircuitInfo.DistributionalDevice.Type);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.SrcDevicePort, out param))
            {
                switch (param.Definition.ParameterType)
                {
                    case ParameterType.Text:
                        ElectricalSystem.get_Parameter(param.Definition).Set(
                            ThisMainData.PortNumberingData.SrcType == PortNumberType.Description
                                ? DataCircuitInfo.DistributionalDevice.ConnectorDescription
                                : DataCircuitInfo.DistributionalDevice.AggregatedPortNumber.ToString()
                        );
                        break;
                    case ParameterType.Integer:
                    {
                        int port;
                        try
                        {
                            port = ThisMainData.PortNumberingData.SrcType == PortNumberType.Description
                                ? int.Parse(DataCircuitInfo.DistributionalDevice.ConnectorDescription)
                                : DataCircuitInfo.DistributionalDevice.AggregatedPortNumber;
                        }
                        catch (Exception e)
                        {
                            port = -1;
                        }

                        ElectricalSystem.get_Parameter(param.Definition).Set(port);
                        break;
                    }
                }
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.DstDeviceId, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.TargetDevice.Name);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.DstDeviceType, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.TargetDevice.Type);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.DstDevicePort, out param))
            {
                switch (param.Definition.ParameterType)
                {
                    case ParameterType.Text:
                        ElectricalSystem.get_Parameter(param.Definition).Set(
                            ThisMainData.PortNumberingData.DstType == PortNumberType.Description
                                ? DataCircuitInfo.TargetDevice.ConnectorDescription
                                : DataCircuitInfo.TargetDevice.AggregatedPortNumber.ToString()
                        );
                        break;
                    case ParameterType.Integer:
                    {
                        int port;
                        try
                        {
                            port = ThisMainData.PortNumberingData.DstType == PortNumberType.Description
                                ? int.Parse(DataCircuitInfo.TargetDevice.ConnectorDescription)
                                : DataCircuitInfo.TargetDevice.AggregatedPortNumber;
                        }
                        catch (Exception e)
                        {
                            port = -1;
                        }

                        ElectricalSystem.get_Parameter(param.Definition).Set(port);
                        break;
                    }
                }

            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.CableType, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.CableType);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.OutputParametersData.CableCapacity, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.CableCapacity);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.CableData.SrcCableMarkParameter, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.CableMarkOnDistributional);
            }

            if (CircuitParameters.Parameters.TryGetValue(ThisMainData.CableData.DstCableMarkParameter, out param))
            {
                ElectricalSystem.get_Parameter(param.Definition).Set(DataCircuitInfo.CableMarkOnTarget);
            }
        }

        private DataCircuitInfo CreateDataCircuitInfo(DeviceInfo Distribution, DeviceInfo Target)
        {
            var info = new DataCircuitInfo(Distribution, Target)
            {
                CableType = ThisMainData.CableData.CableType,
                CableCapacity = ThisMainData.CableData.CableCapacity
            };
            info.CableMarkOnDistributional = ThisMainData.CableData.SrcCableMarkTemplate
                .Replace("${src.name}", info.DistributionalDevice.Name)
                .Replace("${src.port}",
                    ThisMainData.PortNumberingData.SrcType == PortNumberType.CircuitNumber
                        ? info.DistributionalDevice.AggregatedPortNumber.ToString()
                        : info.DistributionalDevice.ConnectorDescription)
                .Replace("${dst.name}", info.TargetDevice.Name)
                .Replace("${dst.port}", ThisMainData.PortNumberingData.DstType == PortNumberType.CircuitNumber
                    ? info.TargetDevice.AggregatedPortNumber.ToString()
                    : info.TargetDevice.ConnectorDescription);
            info.CableMarkOnTarget = ThisMainData.CableData.DstCableMarkTemplate
                .Replace("${src.name}", info.DistributionalDevice.Name)
                .Replace("${src.port}",
                    ThisMainData.PortNumberingData.SrcType == PortNumberType.CircuitNumber
                        ? info.DistributionalDevice.AggregatedPortNumber.ToString()
                        : info.DistributionalDevice.ConnectorDescription)
                .Replace("${dst.name}", info.TargetDevice.Name)
                .Replace("${dst.port}", ThisMainData.PortNumberingData.DstType == PortNumberType.CircuitNumber
                    ? info.TargetDevice.AggregatedPortNumber.ToString()
                    : info.TargetDevice.ConnectorDescription);

            return info;
        }

        private DeviceInfo ExtractDeviceInfo(ElectricalSystem system, ElementId deviceId)
        {
            var device = new DeviceInfo();
            var element = (FamilyInstance) ThisExtension.Revit.ActiveUIDocument.Document.GetElement(deviceId);
            var family = CircuitFamilies.Families[element.Symbol.FamilyName];
            device.Name = family
                .Parameters.TryGetValue(
                    ThisMainData.InputParametersData.Parameters[element.Symbol.FamilyName].DeviceName,
                    out var nameParameter)
                ? nameParameter.StringValue(element)
                : "unknown";
            device.Type = family
                .Parameters.TryGetValue(
                    ThisMainData.InputParametersData.Parameters[element.Symbol.FamilyName].DeviceType,
                    out var typeParameter)
                ? typeParameter.StringValue(element)
                : "unknown";
            device.FirstPortNumber = family.Parameters.TryGetValue(
                ThisMainData.InputParametersData.Parameters[element.Symbol.FamilyName].FirstPortNumber,
                out var firstPortNumberParameter)
                ? element.get_Parameter(firstPortNumberParameter.Definition).AsInteger()
                : 1;
            if (device.FirstPortNumber == 0) device.FirstPortNumber = 1;
            var panelConnector = system.ConnectorManager.Connectors.Cast<Connector>()
                .Single(cc => cc.AllRefs.Cast<Connector>()
                    .Any(dv => dv.Owner.Id == deviceId));
            device.ConnectorNumber = deviceId == system.BaseEquipment.Id
                ? int.TryParse(
                    panelConnector.Owner.get_Parameter(BuiltInParameter.RBS_ELEC_CIRCUIT_NUMBER).AsString(),
                    out var v)
                    ? v
                    : -1
                : panelConnector.AllRefs.Cast<Connector>().First().Id;
            device.ConnectorDescription = deviceId == system.BaseEquipment.Id
                ? panelConnector.Owner.get_Parameter(BuiltInParameter.RBS_ELEC_CIRCUIT_NUMBER).AsString()
                : panelConnector.AllRefs.Cast<Connector>()
                    .Single(a => a.Owner.Id == deviceId)
                    .Description;
            device.AggregatedPortNumber =
                (device.ConnectorNumber - 1) / ThisMainData.PortNumberingData.AggregationCoefficient +
                device.FirstPortNumber;
            return device;
        }

        public override bool OnCanClose()
        {
            // insert code here.

            return true;
        }

        public override bool OnCanClose(REX.Common.REXCanCloseType Type)
        {
            // insert code here.

            return true;
        }

        public override void OnClose()
        {
            // insert code here.
        }

        #endregion

        public int SelectedCircuitsCount(string PanelPatternText)
        {
            try
            {
                var regex = new Regex(PanelPatternText);
                return CircuitsSet.Circuits.Count(it => regex.IsMatch(it.PanelName));
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}