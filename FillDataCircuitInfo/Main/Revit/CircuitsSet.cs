﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Electrical;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class CircuitsSet
    {
        public List<ElectricalSystem> Circuits;
        public CircuitsSet(Document Document)
        {
            var collector = new Autodesk.Revit.DB.FilteredElementCollector(Document);
            Circuits = collector.OfClass(typeof(ElectricalSystem))
                .Cast<ElectricalSystem>()
                .Where(it => it.SystemType == ElectricalSystemType.Data)
                .ToList();
        }
    }
}