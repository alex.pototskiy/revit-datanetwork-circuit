﻿using System.Collections.Generic;
using Autodesk.Revit.DB;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class CircuitFamilies
    {
        public readonly Dictionary<string, FamilyInformation> Families = new Dictionary<string, FamilyInformation>();

        public CircuitFamilies(CircuitsSet circuits)
        {
            foreach (var circuit in circuits.Circuits)
            {
                if (!Families.ContainsKey(circuit.BaseEquipment.Symbol.FamilyName))
                    Families[circuit.BaseEquipment.Symbol.FamilyName] = new FamilyInformation(circuit.BaseEquipment);
                foreach (var device in circuit.Elements)
                {
                    if (device is FamilyInstance v && !Families.ContainsKey(v.Symbol.FamilyName))
                    {
                        Families[v.Symbol.FamilyName] = new FamilyInformation(v);
                    }
                }
            }
        }
    }
}