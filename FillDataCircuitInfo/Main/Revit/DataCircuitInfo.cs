﻿using Autodesk.Revit.DB;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class DataCircuitInfo
    {
        public DeviceInfo DistributionalDevice;
        public DeviceInfo TargetDevice;
        public string CableMarkOnDistributional;
        public string CableMarkOnTarget;
        public string CableType;
        public int CableCapacity;

        public DataCircuitInfo(DeviceInfo DistributionalDevice, DeviceInfo TargetDevice)
        {
            this.DistributionalDevice = DistributionalDevice;
            this.TargetDevice = TargetDevice;
        }
    }
}