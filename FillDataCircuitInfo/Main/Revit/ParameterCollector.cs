﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class ParameterCollector
    {
        static public List<ElementId> CollectParameters(Element Element)
        {
            return (from Parameter parameter in Element.Parameters select parameter.Id).ToList();
        }
    }
}