﻿using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;
using REX.FillDataCircuitInfo.Main.MyData;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class FamilyInformation
    {
        public Family Family;

        public readonly Dictionary<string, ParameterDefinition> Parameters =
            new Dictionary<string, ParameterDefinition>();

        public FamilyInformation(FamilyInstance Element)
        {
            this.Family = Element.Symbol.Family;
            var doc = Element.Document;
            var paramList = Element.Parameters.Cast<Parameter>().ToList();
            foreach (var parameter in paramList)
            {
                Parameters[parameter.Definition.Name] = new ParameterDefinition(parameter.Definition,
                    parameter.StorageType, parameter.IsReadOnly);
            }
        }

        public List<string> IntParameterNames =>
            Parameters.Where(it =>
                    it.Value.StorageType == StorageType.Integer &&
                    it.Value.Definition.ParameterType == ParameterType.Integer)
                .Select(it => it.Key)
                .ToList();
    }
}