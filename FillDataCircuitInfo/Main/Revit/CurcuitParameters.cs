﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Revit.DB;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public class CircuitParameters
    {
        public readonly Dictionary<string, ParameterDefinition> Parameters =
            new Dictionary<string, ParameterDefinition>();

        public CircuitParameters(Document Document, CircuitsSet CircuitsSet)
        {
            // var paramList = new List<ElementId>();
            // foreach (var circuit in CircuitsSet.Circuits)
            // {
            //     var current = (from Parameter parameter in circuit.Parameters select parameter.Id).ToList();
            //     paramList = paramList.Count == 0 ? current : paramList.Intersect(current).ToList();
            // }
            if (CircuitsSet.Circuits.Count == 0) return;
            var paramList = CircuitsSet.Circuits.First().Parameters.Cast<Parameter>().ToList();
            foreach (var parameter in paramList)
            {
                Parameters[parameter.Definition.Name] = new ParameterDefinition(
                    parameter.Definition,
                    parameter.StorageType,
                    parameter.IsReadOnly);
            }
        }

        public IEnumerable<string> ParameterNames => Parameters.Keys.OrderBy(it => it);

        public IEnumerable<string> WritableTextParameterNames =>
            Parameters.Where(it =>
                    !it.Value.IsReadOnly && it.Value.StorageType == StorageType.String &&
                    it.Value.Definition.ParameterType == ParameterType.Text)
                .Select(it => it.Key)
                .OrderBy(it => it);
        
        public IEnumerable<string> WritableIntTextParameterNames =>
            Parameters.Where(it =>
                    !it.Value.IsReadOnly && (it.Value.StorageType == StorageType.String ||
                                             it.Value.StorageType == StorageType.Integer) &&
                    (it.Value.Definition.ParameterType == ParameterType.Text ||
                     it.Value.Definition.ParameterType == ParameterType.Integer))
                .Select(it => it.Key)
                .OrderBy(it => it);

        public IEnumerable<string> WritableIntParameters =>
            Parameters.Where(it =>
                    !it.Value.IsReadOnly && it.Value.StorageType == StorageType.Integer &&
                    it.Value.Definition.ParameterType == ParameterType.Integer)
                .Select(it => it.Key)
                .OrderBy(it => it);
    }
}