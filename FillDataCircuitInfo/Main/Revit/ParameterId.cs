﻿using System;
using Autodesk.Revit.DB;

namespace REX.FillDataCircuitInfo.Main.Revit
{
    public abstract class ParameterId
    {
        public abstract Parameter GetParameter(Element Element);

        public string StringValue(Element Element)
        {
            var param = GetParameter(Element);
            switch (param.StorageType)
            {
                case StorageType.Integer:
                case StorageType.Double:
                    return param.AsValueString();
                case StorageType.String:
                    return param.AsString();
                case StorageType.ElementId:
                    return Element.Document.GetElement(param.AsElementId()).Name;
                default:
                    return "";
            }
        }
    }

    public class BuiltInParameterId : ParameterId
    {
        public BuiltInParameter BuiltInParameter;

        public BuiltInParameterId(BuiltInParameter BuiltInParameter)
        {
            this.BuiltInParameter = BuiltInParameter;
        }

        public override Parameter GetParameter(Element Element)
        {
            return Element.get_Parameter(BuiltInParameter);
        }
    }

    public class ParameterDefinition : ParameterId
    {
        public Definition Definition;
        public StorageType StorageType;
        public bool IsReadOnly;

        public ParameterDefinition(Definition Definition, StorageType StorageType, bool IsReadOnly)
        {
            this.Definition = Definition;
            this.StorageType = StorageType;
            this.IsReadOnly = IsReadOnly;
        }

        public override Parameter GetParameter(Element Element)
        {
            return Element.get_Parameter(Definition);
        }
    }
}