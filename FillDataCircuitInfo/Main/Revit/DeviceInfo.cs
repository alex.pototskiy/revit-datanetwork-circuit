﻿namespace REX.FillDataCircuitInfo.Main.Revit
{
    public struct DeviceInfo
    {
        public string Name;
        public string Type;
        public int ConnectorNumber;
        public string ConnectorDescription;
        public int FirstPortNumber;
        public int AggregatedPortNumber;
    }
}