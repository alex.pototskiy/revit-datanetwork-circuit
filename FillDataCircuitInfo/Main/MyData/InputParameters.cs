﻿namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class InputParameters
    {
        public string DeviceName = "";
        public string DeviceType = "";
        public string FirstPortNumber = "";
    }
}