﻿namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class CableData
    {
        public string CableType = "unknown";
        public int CableCapacity = 1;
        public bool IsSrcDstTemplateTheSame = true;
        public string SrcCableMarkTemplate="${src.name}.${src.port}-${dst.name}.${dst.port}";
        public string DstCableMarkTemplate="${src.name}.${src.port}-${dst.name}.${dst.port}";
        public string SrcCableMarkParameter = "";
        public string DstCableMarkParameter = "";
    }
}