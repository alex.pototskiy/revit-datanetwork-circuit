﻿namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class PortNumberingData
    {
        public int AggregationCoefficient = 1;
        public bool IsScrDstPortNumberingTheSame = true;
        public PortNumberType SrcType = PortNumberType.Description;
        public PortNumberType DstType = PortNumberType.Description;
    }
}