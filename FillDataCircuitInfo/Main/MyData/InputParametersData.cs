﻿using System.Collections.Generic;

namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class InputParametersData
    {
        public readonly Dictionary<string, InputParameters> Parameters = new Dictionary<string,InputParameters>();
    }
}