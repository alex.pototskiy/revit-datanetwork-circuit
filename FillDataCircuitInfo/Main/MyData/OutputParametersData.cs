﻿using System.Collections.Generic;

namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class OutputParametersData
    {
        public string SrcDeviceId = "";
        public string SrcDeviceType = "";
        public string SrcDevicePort = "";
        public string DstDeviceId = "";
        public string DstDeviceType = "";
        public string DstDevicePort = "";
        public string CableType = "";
        public string CableCapacity = "";
    }
}