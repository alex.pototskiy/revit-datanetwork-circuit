﻿namespace REX.FillDataCircuitInfo.Main.MyData
{
    public class SelectionData
    {
        public SelectionType SelectionType = SelectionType.PanelName;
        public string PanelNamePattern = ".*";
    }
}