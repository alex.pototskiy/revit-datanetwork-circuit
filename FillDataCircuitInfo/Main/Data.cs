//
// (C) Copyright 2007-2019 by Autodesk, Inc. All rights reserved.
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM 'AS IS' AND WITH ALL ITS FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE. AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

/// <summary>
/// </summary>

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Autodesk.REX.Framework;
using REX.Common;
using REX.FillDataCircuitInfo.Main.MyData;

namespace REX.FillDataCircuitInfo
{
    [Serializable]
    internal class DataSerializable : REXExtensionDataSerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataSerializable"/> class.
        /// </summary>
        public DataSerializable()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="REXExtensionDataSerializable"/> class.
        /// A required constructor by ISerializable to deserialize the object. It is important to implement
        /// this required constructor. Note that no warning will be given if this constructor is absent and
        /// an exception will be thrown if you attempt to deserialize this object.
        /// It is a good idea to make the constructor protected unless the class was sealed, in which case
        /// the constructor should be marked as private. Making the constructor protected ensures that users
        /// cannot explicitly call this constructor while still allowing derived classes to call it. The same
        /// logic applies if this class was sealed except that there will be no derived classes.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="ctx">The CTX.</param>
        protected DataSerializable(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext ctx)
        {
        }

        /// <summary>
        /// A required method by ISerializable to serialize the object. Simply add variables to be serialized
        /// as name/value pairs to SerializationInfo object. Here you decide which member variables to serialize.
        /// Note that any name can be used to identify each serialized field
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="ctx">The CTX.</param>
        public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext ctx)
        {
        }


        /// <summary>
        /// Sets the data reference.
        /// </summary>
        /// <param name="data">The data.</param>
        public void SetDataReference(Data data)
        {
            DataRef = data;
        }

        private Data DataRef;
    }

    /// <summary>
    /// Main data class to store all data in REX base units.
    /// </summary>
    internal class Data : REXData
    {
        public SelectionData SelectionData = new SelectionData();
        public PortNumberingData PortNumberingData = new PortNumberingData();
        public CableData CableData = new CableData();
        public OutputParametersData OutputParametersData = new OutputParametersData();
        public InputParametersData InputParametersData = new InputParametersData();

        /// <summary>
        /// Initializes a new instance of a <see cref="Data"/> class.
        /// </summary>
        /// <param name="Ext">The reference to REXExtension object.</param>
        public Data(REXExtension Ext)
            : base(Ext)
        {
            VersionCurrent = 1;
            DataSerializableRef = new DataSerializable();
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        public Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Called when set defaults.
        /// </summary>
        /// <param name="UnitsSystem">The units system.</param>
        protected override void OnSetDefaults(REXUnitsSystemType UnitsSystem)
        {
            if (UnitsSystem == REXUnitsSystemType.Imperial)
            {
            }
            else if (UnitsSystem == REXUnitsSystemType.Metric)
            {
            }
        }

        /// <summary>
        /// Called when save data.
        /// </summary>
        /// <param name="Data">The data.</param>
        /// <returns>Returns true if succeeded.</returns>
        protected override bool OnSave(ref BinaryWriter Data)
        {
            if (VersionCurrent < 1) return true;
            ThisExtension.OnSetData();

            #region Write selection configuration

            Data.Write((int) SelectionData.SelectionType);
            Data.Write(SelectionData.PanelNamePattern);

            #endregion

            #region Write port numbering configuration

            Data.Write(PortNumberingData.AggregationCoefficient);
            Data.Write(PortNumberingData.IsScrDstPortNumberingTheSame);
            Data.Write((int) PortNumberingData.SrcType);
            Data.Write((int) PortNumberingData.DstType);

            #endregion

            #region Write cable configuration

            Data.Write(CableData.CableType);
            Data.Write(CableData.CableCapacity);
            Data.Write(CableData.IsSrcDstTemplateTheSame);
            Data.Write(CableData.SrcCableMarkTemplate);
            Data.Write(CableData.SrcCableMarkParameter);
            Data.Write(CableData.DstCableMarkTemplate);
            Data.Write(CableData.DstCableMarkParameter);

            #endregion

            #region Write input parameters configuration

            Data.Write(InputParametersData.Parameters.Count);
            foreach (var parameter in InputParametersData.Parameters)
            {
                Data.Write(parameter.Key);
                Data.Write(parameter.Value.DeviceName);
                Data.Write(parameter.Value.DeviceType);
                Data.Write(parameter.Value.FirstPortNumber);
            }

            #endregion

            #region Write parameter names configuration

            Data.Write(OutputParametersData.SrcDeviceId);
            Data.Write(OutputParametersData.SrcDeviceType);
            Data.Write(OutputParametersData.SrcDevicePort);
            Data.Write(OutputParametersData.DstDeviceId);
            Data.Write(OutputParametersData.DstDeviceType);
            Data.Write(OutputParametersData.DstDevicePort);
            Data.Write(OutputParametersData.CableType);
            Data.Write(OutputParametersData.CableCapacity);

            #endregion

            return true;
        }

        /// <summary>
        /// Called when load data.
        /// </summary>
        /// <param name="Data">The data.</param>
        /// <returns>Returns true if succeeded.</returns>
        protected override bool OnLoad(ref BinaryReader Data)
        {
            switch (Mode)
            {
                case DataMode.ModeFile:
                    break;
                case DataMode.ModeProject:
                    break;
                case DataMode.ModeObject:
                    break;
            }

            if (VersionLoaded < 1) return true;

            #region Load selection configuration

            SelectionData.SelectionType = (SelectionType) Data.ReadInt32();
            SelectionData.PanelNamePattern = Data.ReadString();

            #endregion

            #region Load port numbering configuration

            PortNumberingData.AggregationCoefficient = Data.ReadInt32();
            PortNumberingData.IsScrDstPortNumberingTheSame = Data.ReadBoolean();
            PortNumberingData.SrcType = (PortNumberType) Data.ReadInt32();
            PortNumberingData.DstType = (PortNumberType) Data.ReadInt32();

            #endregion

            #region Load cable configuration

            CableData.CableType = Data.ReadString();
            CableData.CableCapacity = Data.ReadInt32();
            CableData.IsSrcDstTemplateTheSame = Data.ReadBoolean();
            CableData.SrcCableMarkTemplate = Data.ReadString();
            CableData.SrcCableMarkParameter = Data.ReadString();
            CableData.DstCableMarkTemplate = Data.ReadString();
            CableData.DstCableMarkParameter = Data.ReadString();

            #endregion

            #region Load input parameters configuration

            var count = Data.ReadInt32();
            for (var i = 0; i < count; i++)
            {
                var family = Data.ReadString();
                InputParametersData.Parameters[family] = new InputParameters
                {
                    DeviceName = Data.ReadString(),
                    DeviceType = Data.ReadString(),
                    FirstPortNumber =  Data.ReadString()
                };
            }

            #endregion

            #region Load parameter names configuration

            OutputParametersData.SrcDeviceId = Data.ReadString();
            OutputParametersData.SrcDeviceType = Data.ReadString();
            OutputParametersData.SrcDevicePort = Data.ReadString();
            OutputParametersData.DstDeviceId = Data.ReadString();
            OutputParametersData.DstDeviceType = Data.ReadString();
            OutputParametersData.DstDevicePort = Data.ReadString();
            OutputParametersData.CableType = Data.ReadString();
            OutputParametersData.CableCapacity = Data.ReadString();

            #endregion

            ThisExtension.OnSetDialogs();

            return true;
        }

        /// <summary>
        /// Called when save data.
        /// </summary>
        /// <param name="Data">The data.</param>
        /// <returns>Returns true if succeeded.</returns>
        protected override bool OnSave(ref Stream DataStream, ref System.Runtime.Serialization.IFormatter DataFormater)
        {
            if (DataSerializableRef != null)
                DataFormater.Serialize(DataStream, DataSerializableRef);
            return true;
        }

        /// <summary>
        /// Called when load data.
        /// </summary>
        /// <param name="DataStream">The data.</param>
        /// <returns>Returns true if succeeded.</returns>
        protected override bool OnLoad(ref Stream DataStream, ref System.Runtime.Serialization.IFormatter DataFormater)
        {
            DataSerializableRef = (DataSerializable) DataFormater.Deserialize(DataStream);
            return true;
        }

        internal DataSerializable DataSerializableRef;
    }
}