using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using REX.FillDataCircuitInfo.Kotlin;
using REX.FillDataCircuitInfo.Main.Revit;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    /// <summary>
    /// Interaction logic for SubControl.xaml
    /// </summary>
    public partial class CableConfiguration : REX.Common.REXExtensionControl
    {
        public CircuitParameters CircuitParameters { get; set; }

        public CableConfiguration()
        {
            CircuitParameters = ThisMainExtension.RevitExtension.CircuitParameters;
            InitializeComponent();
        }

        public CableConfiguration(REX.Common.REXExtension extension)
            : base(extension)
        {
            CircuitParameters = ThisMainExtension.RevitExtension.CircuitParameters;
            InitializeComponent();
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData
        {
            get { return ThisMainExtension.Data; }
        }

        private void SrcDstSameTemplate_OnChecked(object sender, RoutedEventArgs e)
        {
            DstCableMarkTemplate.Text = SrcCableMarkTemplate.Text;
        }

        private void SrcCableMarkTemplate_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsSrcDstTemplateTheSame.IsChecked == true)
            {
                DstCableMarkTemplate.Text = SrcCableMarkTemplate.Text;
            }
        }

        public void SetDialog()
        {
            var paramNames = SrcCableMarkParameter.ItemsSource.Cast<string>().ToList();
            CableType.Text = ThisMainData.CableData.CableType;
            CableCapacity.DoubleValue = ThisMainData.CableData.CableCapacity;
            IsSrcDstTemplateTheSame.IsChecked = ThisMainData.CableData.IsSrcDstTemplateTheSame;
            SrcCableMarkTemplate.Text = ThisMainData.CableData.SrcCableMarkTemplate;
            SrcCableMarkParameter.SelectedValue = ThisMainData.CableData.SrcCableMarkParameter;
            DstCableMarkTemplate.Text = ThisMainData.CableData.DstCableMarkTemplate;
            DstCableMarkParameter.SelectedValue = ThisMainData.CableData.DstCableMarkParameter;
        }

        public void SetData()
        {
            ThisMainData.CableData.CableType = CableType.Text;
            ThisMainData.CableData.CableCapacity = (int) CableCapacity.DoubleValue;
            ThisMainData.CableData.IsSrcDstTemplateTheSame = IsSrcDstTemplateTheSame.IsChecked ?? false;
            ThisMainData.CableData.SrcCableMarkTemplate = SrcCableMarkTemplate.Text;
            ThisMainData.CableData.SrcCableMarkParameter =
                SrcCableMarkParameter.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.CableData.DstCableMarkTemplate = DstCableMarkTemplate.Text;
            ThisMainData.CableData.DstCableMarkParameter =
                DstCableMarkParameter.SelectedValue?.Let(it => it as string) ?? "";
        }
    }
}