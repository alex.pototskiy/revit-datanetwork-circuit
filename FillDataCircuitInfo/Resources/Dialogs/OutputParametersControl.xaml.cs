using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using REX.FillDataCircuitInfo.Kotlin;
using REX.FillDataCircuitInfo.Main.Revit;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    /// <summary>
    /// Interaction logic for SubControl.xaml
    /// </summary>
    public partial class OutputParametersControl : REX.Common.REXExtensionControl
    {
        public CircuitParameters CircuitParameters { get; set; }

        public OutputParametersControl()
        {
            CircuitParameters = ThisMainExtension.RevitExtension.CircuitParameters;
            InitializeComponent();
        }

        public OutputParametersControl(REX.Common.REXExtension extension)
            : base(extension)
        {
            CircuitParameters = ThisMainExtension.RevitExtension.CircuitParameters;
            InitializeComponent();
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData => ThisMainExtension.Data;

        public void SetDialog()
        {
            SrcDeviceId.SelectedValue = ThisMainData.OutputParametersData.SrcDeviceId;
            SrcDeviceType.SelectedValue = ThisMainData.OutputParametersData.SrcDeviceType;
            SrcDevicePort.SelectedValue = ThisMainData.OutputParametersData.SrcDevicePort;
            DstDeviceId.SelectedValue = ThisMainData.OutputParametersData.DstDeviceId;
            DstDeviceType.SelectedValue = ThisMainData.OutputParametersData.DstDeviceType;
            DstDevicePort.SelectedValue = ThisMainData.OutputParametersData.DstDevicePort;
            CableType.SelectedValue = ThisMainData.OutputParametersData.CableType;
            CableCapacity.SelectedValue = ThisMainData.OutputParametersData.CableCapacity;
        }

        public void SetData()
        {
            ThisMainData.OutputParametersData.SrcDeviceId = SrcDeviceId.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.SrcDeviceType =
                SrcDeviceType.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.SrcDevicePort =
                SrcDevicePort.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.DstDeviceId = DstDeviceId.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.DstDeviceType =
                DstDeviceType.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.DstDevicePort =
                DstDevicePort.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.CableType = CableType.SelectedValue?.Let(it => it as string) ?? "";
            ThisMainData.OutputParametersData.CableCapacity =
                CableCapacity.SelectedValue?.Let(it => it as string) ?? "";
        }
    }
}