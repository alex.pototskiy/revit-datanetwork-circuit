using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using REX.FillDataCircuitInfo.Main.MyData;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    /// <summary>
    /// Interaction logic for SubControl.xaml
    /// </summary>
    public partial class PortConfiguration : REX.Common.REXExtensionControl
    {
        public PortConfiguration()
        {
            InitializeComponent();
        }

        public PortConfiguration(REX.Common.REXExtension extension)
            : base(extension)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData => ThisMainExtension.Data;

        public void SetDialog()
        {
            NumberOfAggregatePorts.Text = ThisMainData.PortNumberingData.AggregationCoefficient.ToString();
            SrcAndDestSameNumbering.IsChecked = ThisMainData.PortNumberingData.IsScrDstPortNumberingTheSame;
            SrcPortNumberType.SelectedIndex = (int) ThisMainData.PortNumberingData.SrcType;
            DstPortNumberType.SelectedIndex = (int) ThisMainData.PortNumberingData.DstType;
        }

        public void SetData()
        {
            ThisMainData.PortNumberingData.AggregationCoefficient = (int) NumberOfAggregatePorts.DoubleValue;
            ThisMainData.PortNumberingData.IsScrDstPortNumberingTheSame = SrcAndDestSameNumbering.IsChecked ?? false;
            ThisMainData.PortNumberingData.SrcType = (PortNumberType) SrcPortNumberType.SelectedIndex;
            ThisMainData.PortNumberingData.DstType = (PortNumberType) DstPortNumberType.SelectedIndex;
        }

        private void SrcPortNumberType_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SrcAndDestSameNumbering.IsChecked == true)
            {
                DstPortNumberType.SelectedIndex = SrcPortNumberType.SelectedIndex;
            }
        }

        private void SrcAndDestSameNumbering_OnChecked(object sender, RoutedEventArgs e)
        {
            DstPortNumberType.SelectedIndex = SrcPortNumberType.SelectedIndex;
        }
    }
}