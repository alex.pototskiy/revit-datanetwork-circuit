﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using REX.Common;
using REX.Controls.WPF;
using REX.FillDataCircuitInfo.Kotlin;
using REX.FillDataCircuitInfo.Main.MyData;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    public partial class InputParametersControl : REXExtensionControl
    {
        public readonly Dictionary<string, InputParameterControls> ParameterControls =
            new Dictionary<string, InputParameterControls>();

        public InputParametersControl()
        {
            InitializeComponent();
        }

        public InputParametersControl(REX.Common.REXExtension extension) : base(extension)
        {
            InitializeComponent();
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension => (Extension) ThisExtension;

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData => ThisMainExtension.Data;

        public void SetDialog()
        {
            foreach (var parameter in ThisMainData.InputParametersData.Parameters)
            {
                if (!ParameterControls.ContainsKey(parameter.Key)) continue;
                var controls = ParameterControls[parameter.Key];
                controls.DeviceName.SelectedValue = parameter.Value.DeviceName;
                controls.DeviceType.SelectedValue = parameter.Value.DeviceType;
                controls.FirstPortNumber.SelectedValue = parameter.Value.FirstPortNumber;
            }
        }

        public void SetData()
        {
            ThisMainData.InputParametersData.Parameters.Clear();
            foreach (var controls in ParameterControls)
            {
                ThisMainData.InputParametersData.Parameters[controls.Key] = new InputParameters
                {
                    DeviceName = controls.Value.DeviceName.SelectedValue?.Let(it => it as string) ?? "",
                    DeviceType = controls.Value.DeviceType.SelectedValue?.Let(it => it as string) ?? "",
                    FirstPortNumber = controls.Value.FirstPortNumber.SelectedValue?.Let(it => it as string) ?? ""
                };
            }
        }
    }
}