﻿using REX.Controls.WPF;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    public struct InputParameterControls
    {
        public REXComboBox DeviceName;
        public REXComboBox DeviceType;
        public REXComboBox FirstPortNumber;
    }
}