using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autodesk.Revit.UI;
using REX.FillDataCircuitInfo.Main.MyData;
using REX.FillDataCircuitInfo.Main.Revit;

namespace REX.FillDataCircuitInfo.Resources.Dialogs
{
    /// <summary>
    /// Interaction logic for SubControl.xaml
    /// </summary>
    public partial class SelectionControl : REX.Common.REXExtensionControl
    {
        public CircuitsSet CircuitsSet { get; set; }

        public SelectionControl()
        {
            CircuitsSet = ThisMainExtension.RevitExtension.CircuitsSet;
            InitializeComponent();
        }

        public SelectionControl(REX.Common.REXExtension extension)
            : base(extension)
        {
            CircuitsSet = ThisMainExtension.RevitExtension.CircuitsSet;
            InitializeComponent();
        }

        public void SetDialog()
        {
            SelectionType.SelectedIndex = (int) ThisMainExtension.Data.SelectionData.SelectionType;
            PanelPattern.Text = ThisMainExtension.Data.SelectionData.PanelNamePattern;
        }

        public void SetData()
        {
            ThisMainData.SelectionData.SelectionType = (SelectionType) SelectionType.SelectedIndex;
            ThisMainExtension.Data.SelectionData.PanelNamePattern = PanelPattern.Text;
        }

        private void PanelPattern_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            SelectionCount.GetBindingExpression(Label.ContentProperty)?.UpdateTarget();
            // var count = ThisMainExtension.RevitExtension.SelectedCircuitsCount(PanelPattern.Text ?? "");
            // SelectionCount.Content = count == -1 ? "Cannot select" : count.ToString() + " circuits are selected";
        }

        /// <summary>
        /// Get the main extension.
        /// </summary>
        /// <value>The main extension.</value>
        internal Extension ThisMainExtension
        {
            get { return (Extension) ThisExtension; }
        }

        /// <summary>
        /// Get the Data structure.
        /// </summary>
        /// <value>The main Data.</value>
        internal Data ThisMainData => ThisMainExtension.Data;

        public string SelectedCircuitsCount
        {
            get
            {
                try
                {
                    var regex = new Regex(PanelPattern.Text);
                    return CircuitsSet.Circuits.Count(it => regex.IsMatch(it.PanelName)).ToString() + " circuits are selected";
                }
                catch (Exception)
                {
                    return "Cannot select circuits";
                }
            }
        }
    }
}