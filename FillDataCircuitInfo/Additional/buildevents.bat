set SOURCE_PATH=
set DESTINATION_PATH=

if not exist "%DESTINATION_PATH%\FillDataCircuitInfo\FillDataCircuitInfo.dll" goto end
if not exist "%SOURCE_PATH%\FillDataCircuitInfo\FillDataCircuitInfo\bin\Debug\FillDataCircuitInfo.dll" goto end

copy "%SOURCE_PATH%\FillDataCircuitInfo\FillDataCircuitInfo\bin\Debug\FillDataCircuitInfo.dll" "%DESTINATION_PATH%\FillDataCircuitInfo\FillDataCircuitInfo.dll"
copy "%SOURCE_PATH%\FillDataCircuitInfo\FillDataCircuitInfo\bin\Debug\FillDataCircuitInfo.pdb" "%DESTINATION_PATH%\FillDataCircuitInfo\FillDataCircuitInfo.pdb"

:end
